const toggle = document.getElementById("myToggle");
const priceBasic = document.getElementById("priceBasic");
const priceProfessional = document.getElementById("priceProfessional");
const priceMaster = document.getElementById("priceMaster");

window.onload = () => {
    toggle.checked = false;
}

toggle.onclick = () => {
    if (toggle.checked) {
        priceBasic.innerHTML = "&dollar;19.99";
        priceProfessional.innerHTML = "&dollar;24.99";
        priceMaster.innerHTML = "&dollar;39.99";

    } else {

        priceBasic.innerHTML = "&dollar;199.99";
        priceProfessional.innerHTML = "&dollar;249.99";
        priceMaster.innerHTML = "&dollar;399.99";
    }
}
